import { Expose } from 'class-transformer'
import { ClassName, FieldName } from '@/airpower/decorator/CustomName'
import { TableField } from '@/airpower/decorator/TableField'
import { FormField } from '@/airpower/decorator/FormField'
import { BaseEntity } from '@/base/BaseEntity'

[[[entity doc]]]
export class [[[entity class name]]] extends BaseEntity {

[[[entity field]]]

}
