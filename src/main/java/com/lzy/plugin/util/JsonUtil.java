package com.lzy.plugin.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

/**
 * @author: liuzeyang
 * @Date: 2024/1/22 15:55
 * @Description:
 */
public class JsonUtil {

    /**
     * Formatted json
     *
     * @param json
     * @return
     */
    public static String toPrettyFormat(Object json) {
        JsonObject jsonObject = new JsonParser().parse(toJson(json)).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(jsonObject);
    }


    /**
     * Convert the object to a json string
     *
     * @param obj
     * @return
     */
    public static String toJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    /**
     * JSON STRING CONVERTED TO OBJECT
     *
     * @param str
     * @param type
     * @return
     */
    public static <T> T fromJson(String str, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(str, type);
    }

    /**
     * JSON STRING CONVERTED TO OBJECT
     *
     * @param str
     * @param type
     * @return
     */
    public static <T> T fromJson(String str, Class<T> type) {
        Gson gson = new Gson();
        return gson.fromJson(str, type);
    }

}
