package com.lzy.plugin.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Description: Copy from https://github.com/organics2016/pojo2json/blob/master/src/main/java/ink/organics/pojo2json/parser/POJO2JSONParserUtils.java
 */
public class POJO2JSONParserUtils {

    public static List<String> arrayTextToList(String text) {

        text = StringUtils.deleteWhitespace(text);

        boolean array = text.length() > 2 &&
                ((text.startsWith("{") && text.endsWith("}")) ||   // Java
                        (text.startsWith("(") && text.endsWith(")"))); // Kotlin
        if (array) {

            return Arrays.stream(text.substring(1, text.length() - 1)
                    .replace("\"", "")
                    .split(","))
                    .collect(Collectors.toList());

        } else if (text.matches("^\"\\w+\"$")) {
            return Stream.of(text.replace("\"", "")).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }
}
