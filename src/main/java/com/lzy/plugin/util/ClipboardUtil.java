package com.lzy.plugin.util;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * @author: liuzeyang
 * @Date: 2024/1/18 21:34
 * @Description: Clipboard util
 */
public class ClipboardUtil {

    /**
     * Set the content to the paste board
     *
     * @param content
     */
    public static void copyToClipboard(String content) {
        StringSelection selection = new StringSelection(content);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }
}
