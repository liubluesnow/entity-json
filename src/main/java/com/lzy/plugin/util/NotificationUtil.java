package com.lzy.plugin.util;

import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.project.Project;

/**
 * @author: liuzeyang
 * @Date: 2024/1/18 21:34
 * @Description: Notification util
 */
public class NotificationUtil {
    private static final NotificationGroup NOTIFICATION_GROUP = new NotificationGroup("jsonJavaModelNotificationGroup", NotificationDisplayType.BALLOON, true);

    /**
     * Error notification
     *
     * @param project
     * @param content
     */
    public static void notifyError(Project project, String content) {
        NOTIFICATION_GROUP.createNotification(content, NotificationType.ERROR).notify(project);
    }

    /**
     * Information notification
     *
     * @param project
     * @param content
     */
    public static void notifyInformation(Project project, String content) {
        NOTIFICATION_GROUP.createNotification(content, NotificationType.INFORMATION).notify(project);
    }

    /**
     * Warning notification
     *
     * @param project
     * @param content
     */
    public static void notifyWarning(Project project, String content) {
        NOTIFICATION_GROUP.createNotification(content, NotificationType.WARNING).notify(project);
    }
}