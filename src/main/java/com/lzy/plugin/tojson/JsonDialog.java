package com.lzy.plugin.tojson;

import com.intellij.json.JsonLanguage;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.editor.impl.SettingsImpl;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.LanguageTextField;
import com.lzy.plugin.util.ClipboardUtil;
import com.lzy.plugin.util.NotificationUtil;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

/**
 * @author: liuzeyang
 * @Date: 2024/1/18 21:34
 * @Description: Displays a dialog for Json data
 */
public class JsonDialog extends DialogWrapper {
    private int width = 500;
    private int height = 40;
    /**
     * Json data
     */
    private String json;
    private Project project;
    private AnActionEvent event;

    protected JsonDialog(String json, Project project, AnActionEvent event) {
        super(true);
        setTitle("Java To JSON");
        this.json = json;
        this.project = project;
        this.event = event;
        init();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        Dimension dimension = new Dimension(width, 400);
        panel.setPreferredSize(dimension);
        LanguageTextField jsonField = new LanguageTextField(JsonLanguage.INSTANCE, project, json) {
            @Override
            protected EditorEx createEditor() {
                EditorEx editor = super.createEditor();
                editor.setVerticalScrollbarVisible(true);
                editor.setHorizontalScrollbarVisible(true);
                if (editor.getSettings() instanceof SettingsImpl) {
                    SettingsImpl settings = (SettingsImpl) editor.getSettings();
                    settings.setLineNumbersShown(true);
                    settings.setAutoCodeFoldingEnabled(true);
                    settings.setFoldingOutlineShown(true);
                    settings.setAllowSingleLogicalLineFolding(true);
                    settings.setRightMarginShown(false);
                }
                return editor;
            }
        };
        jsonField.setOneLineMode(false);
        panel.add(jsonField);

        return panel;
    }

    @Override
    protected JComponent createSouthPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        Dimension panelDimension = new Dimension(width, 50);
        panel.setPreferredSize(panelDimension);
        Dimension copyJButtonDimension = new Dimension(width, 40);
        // Copy button
        JButton copyJButton = new JButton("Copy");
        copyJButton.setPreferredSize(copyJButtonDimension);
        copyJButton.addActionListener(e -> {
            ClipboardUtil.copyToClipboard(json);
            NotificationUtil.notifyInformation(project, "Copy Success");
        });
        panel.add(copyJButton, BorderLayout.NORTH);

        return panel;
    }
}
