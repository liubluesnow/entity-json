package com.lzy.plugin.tojson;

import com.intellij.lang.FileASTNode;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.util.PsiUtil;
import com.lzy.plugin.tojson.parser.Model2JsonParser;
import com.lzy.plugin.util.JsonUtil;
import com.lzy.plugin.util.NotificationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.uast.UastLanguagePlugin;

import java.util.Map;


/**
 * @author: liuzeyang
 * @Date: 2024/1/10 14:02
 * @Description: java model to Json
 */
public class JavaModel2JsonAction extends AnAction {
    private static final Model2JsonParser model2JSONParser = new Model2JsonParser();
    private Project project = null;

    private void init(AnActionEvent e) {
        this.project = e.getData(PlatformDataKeys.PROJECT);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        init(e);
        PsiFile psiFile = getPsiFile(e);
        if (psiFile == null) {
            return;
        }
        if (!uastSupported(psiFile)) {
            NotificationUtil.notifyError(project, "Please select a Java file");
            return;
        }
        // java class to map object
        Map<String, Object> map = model2JSONParser.parseClass(getPsiClass(psiFile));
        // The map object is serialized into formatted json
        String json = JsonUtil.toPrettyFormat(map);
        //json is displayed in the dialog
        JsonDialog jsonDialog = new JsonDialog(json, project, e);
        jsonDialog.show();
    }

    /**
     * Gets the selected file
     *
     * @param event {@link AnActionEvent }
     * @return PsiFile
     */
    private PsiFile getPsiFile(AnActionEvent event) {
        final VirtualFile[] selectFiles = event.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);
        if (selectFiles.length != 1) {
            NotificationUtil.notifyError(project, "Please select a Java file");
            return null;
        }
        if (selectFiles[0].isDirectory()) {
            NotificationUtil.notifyError(project, "Please select a Java file");
            return null;
        }
        return PsiUtil.getPsiFile(project, selectFiles[0]);
    }

    /**
     * Get PsiClass from PsiFile
     *
     * @param psiFile {@link PsiFile}
     * @return PsiClass
     */
    private PsiClass getPsiClass(PsiFile psiFile) {
        if (psiFile == null) {
            return null;
        }
        FileASTNode node = psiFile.getNode();
        PsiElement psi = node.getPsi();
        PsiJavaFile pp = (PsiJavaFile) psi;
        PsiClass[] classes = pp.getClasses();
        return classes[0];
    }


    public boolean uastSupported(@NotNull final PsiFile psiFile) {
        return UastLanguagePlugin.Companion.getInstances()
                .stream()
                .anyMatch(l -> l.isFileSupported(psiFile.getName()));
    }
}