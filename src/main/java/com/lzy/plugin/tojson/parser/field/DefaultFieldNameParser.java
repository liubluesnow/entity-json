package com.lzy.plugin.tojson.parser.field;

import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiAnnotationMemberValue;
import com.intellij.psi.PsiField;
import org.apache.commons.lang3.StringUtils;

/**
 * @author: liuzeyang
 * @Date: 2024/1/22 17:06
 * @Description: Default implementation
 */
public class DefaultFieldNameParser implements FieldNameParser {
    @Override
    public String getFieldName(PsiField field) {
        // Supports annotation renaming field names
        // com.fasterxml.jackson.annotation.JsonProperty；
        // com.alibaba.fastjson.annotation.JSONField
        PsiAnnotation annotation = field.getAnnotation("com.fasterxml.jackson.annotation.JsonProperty");
        if (annotation != null) {
            PsiAnnotationMemberValue value = annotation.findAttributeValue("value");
            String fieldName = value.getText().replaceAll("\"", "");
            if (StringUtils.isNotBlank(fieldName)) {
                return fieldName;
            }
        }

        annotation = field.getAnnotation("com.alibaba.fastjson.annotation.JSONField");
        if (annotation != null) {
            PsiAnnotationMemberValue value = annotation.findAttributeValue("name");
            String fieldName = value.getText().replaceAll("\"", "");
            if (StringUtils.isNotBlank(fieldName)) {
                return fieldName;
            }
        }

        return field.getName();
    }
}
