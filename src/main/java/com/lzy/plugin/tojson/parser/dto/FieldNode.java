package com.lzy.plugin.tojson.parser.dto;

/**
 * @author: liuzeyang
 * @Date: 2024/1/22 13:17
 * @Description: Records the hierarchy of the classes
 */
public class FieldNode {

    private String className;

    private FieldNode parent;

    public FieldNode(String className, FieldNode parent) {
        this.className = className;
        this.parent = parent;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public FieldNode getParent() {
        return parent;
    }

    public void setParent(FieldNode parent) {
        this.parent = parent;
    }
}
