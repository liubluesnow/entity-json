package com.lzy.plugin.tojson.parser;

import com.google.common.collect.Lists;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import com.lzy.plugin.tojson.parser.dto.FieldNode;
import com.lzy.plugin.tojson.parser.field.FieldNameParser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: liuzeyang
 * @Date: 2024/1/21 16:01
 * @Description: java model to json
 */
public class Model2JsonParser {
    public Model2JsonParser() {
    }

    private Map<String, PsiType> getPsiClassGenericsFormPsiClass(PsiClass psiClass) {
        PsiClassType[] extendsListTypes = psiClass.getExtendsListTypes();
        Map<String, PsiType> psiClassGenericsMap = new HashMap<>();
        for (PsiClassType type : extendsListTypes) {
            psiClassGenericsMap.putAll(getPsiClassGenerics(type));
        }
        return psiClassGenericsMap;
    }

    /**
     * 将的类转换成map对象
     *
     * @param psiClass
     * @return
     */
    public Map<String, Object> parseClass(PsiClass psiClass) {
        Map<String, PsiType> psiClassGenericsMap = getPsiClassGenericsFormPsiClass(psiClass);
        return parseClass(psiClass, psiClassGenericsMap, null);
    }

    /**
     * Converts the class to a map object
     *
     * @param psiClass
     * @return
     */
    public Map<String, Object> parseClass(PsiClass psiClass, Map<String, PsiType> psiClassGenericsMap, FieldNode fieldNode) {
        HashMap map = new LinkedHashMap();
        if (psiClass == null) {
            return map;
        }
        String qualifiedName = psiClass.getQualifiedName();
        if (qualifiedName == null) {
            return new HashMap<>();
            // Annotated code can resolve the following situation to some extent
            // public class BaseTreeVo<E extends BaseTreeVo> extends BaseVo {
            //      private List<E> children;
            // }
           /* PsiClassType[] extendsListTypes = psiClass.getExtendsListTypes();
            if (extendsListTypes.length <= 0) {
                return new HashMap<>();
            }
            PsiClassType psiClassType = extendsListTypes[0];
            psiClass = PsiUtil.resolveClassInClassTypeOnly(psiClassType);
            qualifiedName = psiClassType.getCanonicalText();*/
        }
        FieldNode root = new FieldNode(qualifiedName, fieldNode);
        // Circular reference or not
        if (isCircularReference(root)) {
            return new HashMap<>();
        }
        PsiField[] allFields = psiClass.getAllFields();
        for (PsiField field : allFields) {
            parseField(field, psiClass, map, psiClassGenericsMap, root);
        }
        return map;
    }

    /**
     * 是否循环引用
     *
     * @param fieldNode
     * @return
     */
    private boolean isCircularReference(FieldNode fieldNode) {
        FieldNode tmpFieldNode = fieldNode;
        while (tmpFieldNode != null && tmpFieldNode.getParent() != null) {
            if (tmpFieldNode.getParent() != null && tmpFieldNode.getClassName().equals(tmpFieldNode.getParent().getClassName())) {
                return true;
            }
            tmpFieldNode = tmpFieldNode.getParent();
        }
        return false;
    }

    /**
     * Converts the class to a map object
     *
     * @param field
     * @param psiClass
     * @param map
     * @param psiClassGenericsMap
     * @param fieldNode
     */
    private Map<String, Object> parseField(PsiField field,
                                           PsiClass psiClass,
                                           HashMap map,
                                           Map<String, PsiType> psiClassGenericsMap,
                                           FieldNode fieldNode) {
        // JsonIgnore
        if (Model2JsonConfig.jsonIgnoreRuleList.stream().map(e -> e.ignore(psiClass, field)).anyMatch(e -> e == true)) {
            return map;
        }

        map.put(getFieldName(field), parseFieldValue(field, psiClassGenericsMap, fieldNode));

        return map;
    }


    /**
     * 通过字段解析值
     *
     * @param field
     * @param psiClassGenericsMap
     * @return
     */
    private Object parseFieldValue(PsiField field, Map<String, PsiType> psiClassGenericsMap, FieldNode fieldNode) {
        PsiType type = field.getType();
        Object value = parseFieldValue(type, psiClassGenericsMap, fieldNode);
        return value;
    }

    /**
     * Parse values by field type
     *
     * @param type
     * @param psiClassGenericsMap
     * @return
     */
    private Object parseFieldValue(PsiType type, Map<String, PsiType> psiClassGenericsMap, FieldNode fieldNode) {
        Object value = null;
        if (type instanceof PsiPrimitiveType) {
            // Handle basic types，eg:int、long、double、float……
            value = Model2JsonConfig.getPrimitiveTypeValue(type);
        } else if (type instanceof PsiArrayType) {
            PsiType deepComponentType = type.getDeepComponentType();
            if (deepComponentType instanceof PsiPrimitiveType) {
                // Handles arrays of primitive types，eg: int[]、long[]……
                value = Lists.newArrayList(Model2JsonConfig.getPrimitiveTypeValue(deepComponentType));
            } else {
                // Processing object type data，eg:User[]、Student[]……
                value = Lists.newArrayList(parseFieldValue(deepComponentType, getPsiClassGenerics(deepComponentType), fieldNode));
            }
        } else {
            PsiClass psiClass = PsiUtil.resolveClassInClassTypeOnly(type);
            if (psiClass == null) {
                value = null;
            } else {
                if (psiClass.isEnum()) {
                    // Process enumeration type Enum
                    value = Arrays.stream(psiClass.getAllFields())
                            .filter(f -> f instanceof PsiEnumConstant)
                            .findFirst()
                            .map(PsiField::getName)
                            .orElse("");
                } else {
                    // Gets the parent class of a field type and its own full class name
                    List<String> fieldClassQualifiedNameList = getClassQualifiedName(psiClass);
                    // Processing set type eg:List<Test<User>>
                    boolean isCollection = fieldClassQualifiedNameList.stream().anyMatch(Model2JsonConfig.javaCollectionClassQualifiedNameList::contains);
                    if (isCollection) {
                        // Gets the generic type of the collection eg:Test<User>
                        PsiType typeToDeepType = PsiUtil.extractIterableTypeParameter(type, false);
                        if (typeToDeepType == null) {
                            value = Lists.newArrayList();
                        } else {
                            // Handles generics for collections，<T,User>
                            value = Lists.newArrayList(parseFieldValue(typeToDeepType, getPsiClassGenerics(typeToDeepType), fieldNode));
                        }
                    } else {
                        // Handle java common type default values, eg: package type corresponding to basic type, date......
                        value = getJavaCommonTypeValue(fieldClassQualifiedNameList);
                        if (value == null) {
                            // Processing generics
                            PsiType typeToDeepType = psiClassGenericsMap == null ? null : psiClassGenericsMap.get(psiClass.getName());
                            if (typeToDeepType != null) {
                                value = parseFieldValue(typeToDeepType, getPsiClassGenerics(typeToDeepType), fieldNode);
                            } else {
                                PsiClass sbuPsiClass = PsiUtil.resolveClassInClassTypeOnly(type);
                                value = parseClass(sbuPsiClass, getPsiClassGenerics(type), fieldNode);
                            }
                        }
                    }
                }
            }

        }
        return value;
    }

    /**
     * Gets java's built-in default values for common types
     *
     * @param fieldClassQualifiedNameList
     * @return
     */
    private Object getJavaCommonTypeValue(List<String> fieldClassQualifiedNameList) {
        for (String javaCommonType : Model2JsonConfig.javaCommonTypeValeMap.keySet()) {
            if (fieldClassQualifiedNameList.contains(javaCommonType)) {
                return Model2JsonConfig.javaCommonTypeValeMap.get(javaCommonType);
            }
        }
        return null;
    }

    /**
     * Gets the parent class of the specified class and its own full class name
     *
     * @param psiClass
     * @return
     */
    public List<String> getClassQualifiedName(PsiClass psiClass) {
        List<String> fieldClassQualifiedNameList = new ArrayList<>();
        fieldClassQualifiedNameList.add(psiClass.getQualifiedName());
        fieldClassQualifiedNameList.addAll(Arrays.stream(psiClass.getSupers())
                .map(PsiClass::getQualifiedName).collect(Collectors.toList()));
        fieldClassQualifiedNameList = fieldClassQualifiedNameList.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return fieldClassQualifiedNameList;
    }

    private Map<String, PsiType> getPsiClassGenerics(PsiType type) {
        PsiClass psiClass = PsiUtil.resolveClassInClassTypeOnly(type);
        if (psiClass != null) {
            return Arrays.stream(psiClass.getTypeParameters())
                    .map(p -> Pair.of(p, PsiUtil.substituteTypeParameter(type, psiClass, p.getIndex(), false)))
                    .filter(p -> p.getValue() != null)
                    .collect(Collectors.toMap(p -> p.getKey().getName(), Pair::getValue));
        }
        return new HashMap<>();
    }

    /**
     * Get field name
     *
     * @param field
     * @return fieldName
     */
    private String getFieldName(PsiField field) {
        for (FieldNameParser fieldNameParser : Model2JsonConfig.fieldNameParserList) {
            String fieldName = fieldNameParser.getFieldName(field);
            if (StringUtils.isNotBlank(fieldName)) {
                return fieldName;
            }
        }
        return field.getName();
    }
}
