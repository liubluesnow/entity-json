package com.lzy.plugin.tojson.parser.ignore;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;

/**
 * @author: liuzeyang
 * @Date: 2024/1/22 16:54
 * @Description: Json ignore rule
 */
public interface JsonIgnoreRule {
    /**
     * Whether to ignore
     *
     * @param psiClass
     * @param field
     * @return
     */
    boolean ignore(PsiClass psiClass, PsiField field);
}
