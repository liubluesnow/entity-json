package com.lzy.plugin.tojson.parser.ignore;

import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiModifier;
import com.lzy.plugin.util.POJO2JSONParserUtils;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author: liuzeyang
 * @Date: 2024/1/22 16:55
 * @Description: Default implementation
 */
public class DefaultJsonIgnoreRule implements JsonIgnoreRule {
    @Override
    public boolean ignore(PsiClass psiClass, PsiField field) {
        if (field == null) {
            return false;
        }
        // Static field
        if (field.hasModifierProperty(PsiModifier.STATIC)) {
            return true;
        }
        // JsonIgnore
        if (isJsonIgnore(field)) {
            return true;
        }
        if (psiClass != null) {
            PsiAnnotation annotation = psiClass.getAnnotation("com.fasterxml.jackson.annotation.JsonIgnoreProperties");
            if (annotation != null) {
                List<String> ignoreFileNames = POJO2JSONParserUtils.arrayTextToList(annotation.findAttributeValue("value").getText());
                if (CollectionUtils.isNotEmpty(ignoreFileNames)) {
                    return ignoreFileNames.contains(field.getName());
                }
            }
        }
        return false;
    }

    /**
     * Whether to ignore field
     *
     * @param field
     * @return
     */
    private boolean isJsonIgnore(PsiField field) {
        PsiAnnotation annotation = field.getAnnotation("com.fasterxml.jackson.annotation.JsonIgnore");
        return annotation != null;
    }
}
