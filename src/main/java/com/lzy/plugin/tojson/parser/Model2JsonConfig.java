package com.lzy.plugin.tojson.parser;

import com.intellij.psi.PsiType;
import com.lzy.plugin.tojson.parser.field.DefaultFieldNameParser;
import com.lzy.plugin.tojson.parser.field.FieldNameParser;
import com.lzy.plugin.tojson.parser.ignore.DefaultJsonIgnoreRule;
import com.lzy.plugin.tojson.parser.ignore.JsonIgnoreRule;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author: liuzeyang
 * @Date: 2024/1/21 16:01
 * @Description: Model to json config
 */
public class Model2JsonConfig {

    public static final List<JsonIgnoreRule> jsonIgnoreRuleList = new ArrayList<>();
    public static List<FieldNameParser> fieldNameParserList = new LinkedList<>();
    public static List<String> javaCollectionClassQualifiedNameList = new ArrayList<>();
    public static Map<String, Object> javaCommonTypeValeMap = new LinkedHashMap<>();

    static {
        // Initializes the collection type and converts it to an array when the following types are encountered
        javaCollectionClassQualifiedNameList.add("java.lang.Iterable");
        javaCollectionClassQualifiedNameList.add("java.util.Collection");
        javaCollectionClassQualifiedNameList.add("java.util.AbstractCollection");
        javaCollectionClassQualifiedNameList.add("java.util.List");
        javaCollectionClassQualifiedNameList.add("java.util.AbstractList");
        javaCollectionClassQualifiedNameList.add("java.util.Set");
        javaCollectionClassQualifiedNameList.add("java.util.AbstractSet");

        // Initializes some of the basic types commonly found in java
        javaCommonTypeValeMap.put("com.fasterxml.jackson.databind.JsonNode", new Object());
        javaCommonTypeValeMap.put("com.fasterxml.jackson.databind.node.ArrayNode", new ArrayList<>());
        javaCommonTypeValeMap.put("com.fasterxml.jackson.databind.node.ObjectNode", new Object());
        javaCommonTypeValeMap.put("java.lang.Boolean", Boolean.FALSE);
        javaCommonTypeValeMap.put("java.lang.CharSequence", 'c');
        javaCommonTypeValeMap.put("java.lang.Character", 'c');
        javaCommonTypeValeMap.put("java.lang.Double", 0.00d);
        javaCommonTypeValeMap.put("java.lang.Float", 0.0f);
        javaCommonTypeValeMap.put("java.lang.Number", 0);
        javaCommonTypeValeMap.put("java.math.BigDecimal", BigDecimal.ZERO);
        javaCommonTypeValeMap.put("java.time.LocalDate", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        javaCommonTypeValeMap.put("java.time.LocalDateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
        javaCommonTypeValeMap.put("java.time.LocalTime", LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
        javaCommonTypeValeMap.put("java.time.YearMonth", YearMonth.now().format(DateTimeFormatter.ofPattern("yyyy-MM")));
        javaCommonTypeValeMap.put("java.time.ZonedDateTime", ZonedDateTime.now().format(DateTimeFormatter.ISO_DATE));
        javaCommonTypeValeMap.put("java.time.temporal.Temporal", null);
        javaCommonTypeValeMap.put("java.util.AbstractMap", new HashMap<>());
        javaCommonTypeValeMap.put("java.util.Date", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
        javaCommonTypeValeMap.put("java.util.Map", new HashMap<>());
        javaCommonTypeValeMap.put("java.util.UUID", UUID.randomUUID().toString());

        // Initializes the field ignore rule
        jsonIgnoreRuleList.add(new DefaultJsonIgnoreRule());
        // Initializes the field alias resolution rule
        fieldNameParserList.add(new DefaultFieldNameParser());
    }

    /**
     * Gets the default value for the base type
     *
     * @param type
     * @return
     */
    public static Object getPrimitiveTypeValue(PsiType type) {
        switch (type.getCanonicalText()) {
            case "boolean":
                return false;
            case "byte":
            case "short":
            case "int":
            case "long":
                return 0L;
            case "float":
                return 0.0;
            case "double":
                return 0.00d;
            case "char":
                return 'c';
            default:
                return null;
        }
    }
}
