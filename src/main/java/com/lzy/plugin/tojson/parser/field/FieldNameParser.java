package com.lzy.plugin.tojson.parser.field;

import com.intellij.psi.PsiField;

/**
 * @author: liuzeyang
 * @Date: 2024/1/22 17:05
 * @Description: Parse field name
 */
public interface FieldNameParser {
    /**
     * Parse field name
     *
     * @param field
     * @return
     */
    String getFieldName(PsiField field);
}
