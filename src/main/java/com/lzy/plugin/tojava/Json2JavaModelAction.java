package com.lzy.plugin.tojava;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.lzy.plugin.util.NotificationUtil;
import org.jetbrains.annotations.NotNull;

/**
 * @author Administrator
 */
public class Json2JavaModelAction extends AnAction {

    private Project project = null;
    private VirtualFile virtualFile;


    private void init(AnActionEvent e) {
        this.project = e.getData(PlatformDataKeys.PROJECT);
        this.virtualFile = e.getData(CommonDataKeys.VIRTUAL_FILE);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        init(e);
        VirtualFile actionFolder = e.getData(LangDataKeys.VIRTUAL_FILE);
        actionFolder = actionFolder.isDirectory() ? actionFolder : actionFolder.getParent();
        if (actionFolder.isDirectory()) {
            VirtualFile moduleSourceRoot = ProjectRootManager.getInstance(project).getFileIndex().getSourceRootForFile(actionFolder);
            String packageName = ProjectRootManager.getInstance(project).getFileIndex().getPackageNameByDirectory(actionFolder);
            JavaModelDialog jsonDialog = new JavaModelDialog(project, e, moduleSourceRoot, packageName);
            jsonDialog.show();
        } else {
            NotificationUtil.notifyInformation(project, "Please select a directory");
        }
    }


}
