package com.lzy.plugin.tojava;

import com.alibaba.fastjson.JSON;
import com.intellij.ide.projectView.ProjectView;
import com.intellij.json.JsonLanguage;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.editor.impl.SettingsImpl;
import com.intellij.openapi.fileTypes.FileTypes;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.EditorTextField;
import com.intellij.ui.LanguageTextField;
import com.intellij.ui.components.JBCheckBox;
import com.lzy.plugin.util.JsonUtil;
import com.lzy.plugin.util.NotificationUtil;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

/**
 * @author: liuzeyang
 * @Date: 2024/1/18 21:34
 * @Description: Json to Java model
 */
public class JavaModelDialog extends DialogWrapper {

    private int width = 500;
    private int height = 40;
    private LanguageTextField jsonField;
    private EditorTextField classNameField;
    private JBCheckBox lombokCheckBox;
    private JBCheckBox innerClassCheckBox;

    private Project project;
    private AnActionEvent event;
    private VirtualFile moduleSourceRoot;
    private String packageName;

    public JavaModelDialog(Project project, AnActionEvent e, VirtualFile moduleSourceRoot, String packageName) {
        super(true);
        setTitle("JSON To Java Class");
        this.project = project;
        this.event = event;
        this.moduleSourceRoot = moduleSourceRoot;
        this.packageName = packageName;
        init();
    }

    @Override
    protected @Nullable JComponent createNorthPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(width, 25));
        Document document = EditorFactory.getInstance().createDocument("");
        classNameField = new EditorTextField(document, project, FileTypes.PLAIN_TEXT, false, false) {
            @Override
            protected EditorEx createEditor() {
                EditorEx editor = super.createEditor();
                editor.setVerticalScrollbarVisible(true);
                editor.setHorizontalScrollbarVisible(true);
                editor.setShowPlaceholderWhenFocused(true);
                editor.setPlaceholder("Please enter a java class name");
                return editor;
            }
        };
        panel.add(classNameField);
        return panel;
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(width, 400));
        jsonField = new LanguageTextField(JsonLanguage.INSTANCE, project, "") {
            @Override
            protected EditorEx createEditor() {
                EditorEx editor = super.createEditor();
                editor.setVerticalScrollbarVisible(true);
                editor.setHorizontalScrollbarVisible(true);
                if (editor.getSettings() instanceof SettingsImpl) {
                    SettingsImpl settings = (SettingsImpl) editor.getSettings();
                    settings.setLineNumbersShown(true);
                    settings.setAutoCodeFoldingEnabled(true);
                    settings.setFoldingOutlineShown(true);
                    settings.setAllowSingleLogicalLineFolding(true);
                    settings.setRightMarginShown(true);
                }
                return editor;
            }
        };
        jsonField.setOneLineMode(false);
        panel.add(jsonField);
        return panel;
    }

    @Override
    protected JComponent createSouthPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        Dimension panelDimension = new Dimension(width, 80);
        panel.setPreferredSize(panelDimension);
        Dimension copyJButtonDimension = new Dimension(width / 2, 40);

        // Format button
        JButton formatJButton = new JButton("Format");
        formatJButton.setPreferredSize(copyJButtonDimension);
        formatJButton.addActionListener(e -> {
            String jsonContent = getPrettyJsonContent();
            if (StringUtils.isNotBlank(jsonContent)) {
                jsonField.setText(jsonContent);
            }
        });
        panel.add(formatJButton, BorderLayout.WEST);

        // Create button
        JButton copyJButton = new JButton("Create");
        copyJButton.setPreferredSize(copyJButtonDimension);
        copyJButton.addActionListener(e -> {
            NotificationUtil.notifyInformation(project, "Create Success");
            String prettyJsonContent = getPrettyJsonContent();
            String className = getClassName();

            if (StringUtils.isNotBlank(prettyJsonContent) && StringUtils.isNotBlank(className)) {
                createJavaFile(className, prettyJsonContent);
            }
        });
        panel.add(copyJButton, BorderLayout.EAST);
        // setting
        createSettingsPanel(panel);
        return panel;
    }

    private void createJavaFile(String className, String prettyJsonContent) {
        // Show background process indicator
        ProgressManager.getInstance().run(new Task.Backgroundable(project, "Json To Model Class Generation", false) {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                // Generate POJOs
                GeneratePojos generatePojos = new GeneratePojos(packageName, moduleSourceRoot, indicator, isLombok());
                generatePojos.generateFromJson(className, prettyJsonContent, false, false);

                // Refresh UI
                try {
                    Thread.sleep(100);
                    ProjectView.getInstance(project).refresh();
                    moduleSourceRoot.refresh(false, true);
                } catch (InterruptedException ignored) {
                }
            }
        });
    }

    /**
     * setting
     *
     * @param panel
     */
    private void createSettingsPanel(JPanel panel) {
        // setting
        JPanel settingsPanel = new JPanel();
        settingsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        lombokCheckBox = new JBCheckBox("Lombok");
        lombokCheckBox.setMnemonic('l');
        lombokCheckBox.setFocusable(false);
        settingsPanel.add(lombokCheckBox);

/*        innerClassCheckBox = new JBCheckBox("Inner class");
        innerClassCheckBox.setMnemonic('l');
        innerClassCheckBox.setFocusable(false);
        settingsPanel.add(innerClassCheckBox);*/

        panel.add(settingsPanel, BorderLayout.NORTH);
    }

    /**
     * Get JSON content
     *
     * @return
     */
    private String getPrettyJsonContent() {
        String json = jsonField.getText();
        if (StringUtils.isNotBlank(json)) {
            try {
                HashMap hashMap = JSON.parseObject(json, HashMap.class);
                return JsonUtil.toPrettyFormat(hashMap);
            } catch (Exception ex) {
                NotificationUtil.notifyInformation(project, "Format Exception");
            }
        }
        return null;
    }

    /**
     * Determine whether to use Lombok
     *
     * @return
     */
    private Boolean isLombok() {
        return lombokCheckBox.isSelected();
    }


    /**
     * Whether to use inner classes
     *
     * @return
     */
    private Boolean isInnerClass() {
        return innerClassCheckBox.isSelected();
    }

    /**
     * Get class name
     *
     * @return
     */
    private String getClassName() {
        String className = classNameField.getText();
        if (StringUtils.isBlank(className)) {
            NotificationUtil.notifyInformation(project, "Please enter a java class name");
        }
        return className;
    }

}
