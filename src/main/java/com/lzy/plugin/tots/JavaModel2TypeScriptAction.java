package com.lzy.plugin.tots;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.lang.FileASTNode;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import com.lzy.plugin.util.NotificationUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * @author: liuzeyang
 * @Date: 2024/1/10 14:02
 * @Description: java model to TypeScrip
 */
public class JavaModel2TypeScriptAction extends AnAction {
    Project project = null;
    public static Map<String, String> javaCommonTypeValeMap = new LinkedHashMap<>();
    public static List<String> excludeFieldNameList = new ArrayList();

    private static final Pattern pattern = Pattern.compile("\\w+$");

    static {
        javaCommonTypeValeMap.put("java.lang.Boolean", "boolean");
        javaCommonTypeValeMap.put("boolean", "boolean");
        javaCommonTypeValeMap.put("java.lang.CharSequence", "string");
        javaCommonTypeValeMap.put("java.lang.Character", "string");
        javaCommonTypeValeMap.put("java.lang.Double", "number");
        javaCommonTypeValeMap.put("double", "number");
        javaCommonTypeValeMap.put("java.lang.Float", "number");
        javaCommonTypeValeMap.put("float", "number");
        javaCommonTypeValeMap.put("java.lang.Number", "number");
        javaCommonTypeValeMap.put("java.lang.Long", "number");
        javaCommonTypeValeMap.put("java.lang.Integer", "number");
        javaCommonTypeValeMap.put("int", "number");
        javaCommonTypeValeMap.put("long", "number");
        javaCommonTypeValeMap.put("java.math.BigDecimal", "number");

        javaCommonTypeValeMap.put("java.sql.Timestamp", "string");
        javaCommonTypeValeMap.put("java.sql.Date", "string");
        javaCommonTypeValeMap.put("java.util.Date", "string");
        javaCommonTypeValeMap.put("java.time.LocalDate", "string");
        javaCommonTypeValeMap.put("java.time.LocalDateTime", "string");


        excludeFieldNameList.add("createdTime");
        excludeFieldNameList.add("createdBy");
        excludeFieldNameList.add("updatedBy");
        excludeFieldNameList.add("updatedTime");
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        project = e.getProject();
        String basePath = project.getBasePath();
        String typeScriptDir = basePath + "/typeScript";
        File file = new File(typeScriptDir);
        if (!file.exists()) {
            file.mkdir();
        }
        List<PsiFile> psiFileList = getPsiFile(e);
        // 读取事件目录下的所有文件
        for (PsiFile itemFile : psiFileList) {
            createTsFile(itemFile, typeScriptDir);
        }

        refreshProject(project);
    }

    private void createTsFile(PsiFile psiFile, String typeScriptDir) {
        // 读取模板文件
        String templateContent = readTemplate();
        if (psiFile == null) {
            return;
        }
        PsiClass psiClass = getPsiClass(psiFile);
        String classDocText = psiClass.getDocComment().getText();
        String className = getClassName(psiClass);
        List<String> fieldStringList = getFieldStringList(psiClass);

        // 替换模板中的占位符
        templateContent = templateContent.replace("[[[entity doc]]]", classDocText);
        templateContent = templateContent.replace("[[[entity class name]]]", className);
        templateContent = templateContent.replace("[[[entity field]]]", StringUtils.join(fieldStringList.toArray(new String[0])));

        File tsFile = new File(typeScriptDir + "/" + className + ".ts");
        Writer writer = null;
        try {
            // 检查目标文件是否存在，不存在则创建
            if (!tsFile.exists()) {
                tsFile.createNewFile();// 创建目标文件
            } else {
                tsFile.delete();
            }
            // 向目标文件中写入内容
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tsFile), "UTF-8"));
            //writer = new FileWriter(tsFile, true);
            writer.append(templateContent);
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
    }

    /**
     * string -> java.lang.String
     * number -> java.lang.Long、java.lang.Integer、java.lang.Double、java.lang.Float
     * boolean -> java.lang.Boolean
     * Object  -> Object
     * [](array)  -> collection
     */
    private List<String> getFieldStringList(PsiClass psiClass) {
        List<String> fieldString = new ArrayList<>();
        for (PsiField psiField : psiClass.getAllFields()) {
            if (excludeFieldNameList.contains(psiField.getName())) {
                continue;
            }
            String[] split = psiField.getDocComment() == null ? null : psiField.getDocComment().getText().split("\n");
            String fieldNameDoc = "";
            if (split != null && split.length >= 3) {
                fieldNameDoc = split[1].replace("*", "").trim();
            }
            //String tsType = null;
            PsiType type = psiField.getType();
            //String qualifiedName = PsiTypesUtil.getPsiClass(type).getQualifiedName();

            List<String> classQualifiedName = new ArrayList<>();
            if (type instanceof PsiPrimitiveType) {
                // Primitive type
                classQualifiedName.add(((PsiPrimitiveType) type).getName());
                fieldString.add(createOneTsField(fieldNameDoc, psiField.getName(), javaCommonTypeValeMap.get(type.getCanonicalText())));
            } else if (type.getArrayDimensions() > 0) {
                // Array type
                int arrayDimensions = type.getArrayDimensions();
                String arraySuffix = "";
                for (int i = 0; i < arrayDimensions; i++) {
                    arraySuffix = arraySuffix + "[]";
                }
                String javaType = type.getCanonicalText().split("\\[")[0];
                String tsType = javaCommonTypeValeMap.get(javaType);
                if (tsType == null) {
                    tsType = getSimpleClassName(javaType);
                }
                // long[]、long[][]、Long[]、Long[][]、Student[]、Student[][]……
                fieldString.add(createOneTsField(fieldNameDoc, psiField.getName(), tsType + arraySuffix));
            } else if (isCollection(type)) {
                // Collection type
                // java.util.List<com.xxx.Student<com.xxMonitor>>、java.util.List<com.xxLong> => Student、Long => Student、number
                try {
                    String javaType = type.getCanonicalText().split("<")[1].split(">")[0];
                    String tsType = "";
                    if (javaCommonTypeValeMap.get(javaType) != null) {
                        tsType = javaCommonTypeValeMap.get(javaType);
                    } else {
                        tsType = getSimpleClassName(javaType);
                    }
                    fieldString.add(createOneTsField(fieldNameDoc, psiField.getName(), tsType + "[]"));
                } catch (Exception e) {
                    fieldString.add(createOneTsField(fieldNameDoc, psiField.getName(), "Object" + "[]"));
                }
            } else {
                String javaType = type.getCanonicalText();
                String tsType = getTsTypeFromJavaCommonTypeValeMap(type);
                if (tsType == null) {
                    tsType = getSimpleClassName(javaType);
                }
                fieldString.add(createOneTsField(fieldNameDoc, psiField.getName(), tsType));
            }
        }
        return fieldString;
    }

    private String createOneTsField(String fieldNameDoc, String exposeFieldName, String tsType) {
        String fieldPlate = "  @FieldName('" + fieldNameDoc + "')\n" +
                "  @Expose() " + exposeFieldName + "!: " + tsType + "\n\n";
        return fieldPlate;
    }

    public static String getTsTypeFromJavaCommonTypeValeMap(PsiType psiType) {
        List<String> allSupperClassType = getAllSupperClassType(psiType);
        for (String type : allSupperClassType) {
            String s = javaCommonTypeValeMap.get(type);
            if (s != null) {
                return s;
            }
        }

        return null;
    }

    public static List<String> getAllSupperClassType(PsiType psiType) {
        List<String> currentTypeNames = new ArrayList<>();

        String currentTypeName = psiType.getCanonicalText();
        currentTypeNames.add(currentTypeName);
        for (PsiType superType : psiType.getSuperTypes()) {
            currentTypeNames.add(superType.getCanonicalText());
        }
        return currentTypeNames;
    }

    public static boolean isCollection(PsiType psiType) {
        // java.util.List<com.zly.dto.Student>
        // java.util.Collection<com.zly.dto.Student>
        List<String> collectionClassNames = new ArrayList<>();
        collectionClassNames.add("java.util.List");
        collectionClassNames.add("java.util.Set");
        collectionClassNames.add("java.util.Collection");

        List<String> currentTypeNames = getAllSupperClassType(psiType);
        for (PsiType superType : psiType.getSuperTypes()) {
            currentTypeNames.add(superType.getCanonicalText());
        }

        for (String collectionClassName : collectionClassNames) {
            for (String typeName : currentTypeNames) {
                if (typeName.startsWith(collectionClassName)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String getSimpleClassName(String fullClassName) {
        if (fullClassName != null && fullClassName.contains("<")) {
            fullClassName = fullClassName.split("<")[0];
        }
        Matcher matcher = pattern.matcher(fullClassName);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return null;
        }
    }

    /**
     * Gets the parent class of the specified class and its own full class name
     *
     * @param psiClass
     * @return
     */
    public List<String> getClassQualifiedName(PsiClass psiClass) {
        List<String> fieldClassQualifiedNameList = new ArrayList<>();
        fieldClassQualifiedNameList.add(psiClass.getQualifiedName());
        fieldClassQualifiedNameList.addAll(Arrays.stream(psiClass.getSupers())
                .map(PsiClass::getQualifiedName).collect(Collectors.toList()));
        fieldClassQualifiedNameList = fieldClassQualifiedNameList.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return fieldClassQualifiedNameList;
    }

    private String getClassName(PsiClass psiClass) {
        String name = psiClass.getName();
      /*  name = name.replace("VO", "")
                .replace("vo", "")
                .replace("Vo", "")
                + "Entity";*/
        return name;
    }

    private String readTemplate() {
        //获取文件的URL
        String content = "";
        try {
            InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("TypeScriptEntityTemplate.txt");
            List<String> stringList = IOUtils.readLines(resourceAsStream, StandardCharsets.UTF_8);
            content = StringUtils.join(stringList, "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content;
    }


    /**
     * Get PsiClass from PsiFile
     *
     * @param psiFile {@link PsiFile}
     * @return PsiClass
     */
    private PsiClass getPsiClass(PsiFile psiFile) {
        if (psiFile == null) {
            return null;
        }
        FileASTNode node = psiFile.getNode();
        PsiElement psi = node.getPsi();
        PsiJavaFile pp = (PsiJavaFile) psi;
        PsiClass[] classes = pp.getClasses();
        return classes[0];
    }

    /**
     * Gets the selected file
     *
     * @param event {@link AnActionEvent }
     * @return PsiFile
     */
    private List<PsiFile> getPsiFile(AnActionEvent event) {
        final VirtualFile[] selectFiles = event.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);
        List<PsiFile> psiFileList = new ArrayList<>();
        if (selectFiles.length != 1) {
            NotificationUtil.notifyError(project, "Please select a Java file");
            return null;
        }
        if (selectFiles[0].isDirectory()) {
            VirtualFile[] children = selectFiles[0].getChildren();
            for (VirtualFile child : children) {
                FileType fileType = child.getFileType();
                if (fileType instanceof JavaFileType) {
                    psiFileList.add(PsiUtil.getPsiFile(project, child));
                }
            }
            return psiFileList;
        }
        if (selectFiles[0].getFileType() instanceof JavaFileType) {
            psiFileList.add(PsiUtil.getPsiFile(project, selectFiles[0]));
        }
        return psiFileList;
    }


    public static void refreshProject(Project project) {
        VirtualFile baseDir = ProjectUtil.guessProjectDir(project);
        if (baseDir != null) {
            baseDir.refresh(false, true);
        }
    }
}